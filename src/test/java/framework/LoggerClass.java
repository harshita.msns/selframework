package framework;

import java.util.logging.Logger;

public class LoggerClass extends Wait {
    private static Logger logger;

    public Logger setInstanceOfLogger() {
        logger = Logger.getLogger(this.getClass().getName());
        return logger;
    }

    public static void printInfoMessage(String message) {
        logger.info(message);
    }
}
