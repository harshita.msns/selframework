package framework;

import pages.SearchPage;

public class PageFactory extends LoggerClass {
    private static SearchPage searchPage;

    public static void setInstancesOfPages() {
        searchPage = new SearchPage();
    }

    public static SearchPage getSearchPage() {
        return searchPage;
    }
}
