package framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeSuite;
import java.util.logging.Logger;

public class BaseTest extends PageFactory {
    private static final String URL = "https://www.google.com";
    WebDriver driver;
    WebDriverWait wait;
    Logger logger;

    @BeforeSuite(alwaysRun =true)
    public void setUp() {
        driver = getInstanceOfDriver();
        wait = getInstanceOfWait();
        logger = setInstanceOfLogger();
        PageFactory.setInstancesOfPages();
        driver.manage().window().maximize();
        getURL();
    }

    public void getURL() {
        driver.get(URL);
    }

    public String getTitle() {
        return driver.getTitle();
    }

    @AfterSuite(alwaysRun = true)
    public void tearDown() {
        driver.quit();
    }

    @AfterMethod(alwaysRun = true)
    public void screenshot(ITestResult result)
    {
        String x = "Screenshot"+System.currentTimeMillis();
        if(ITestResult.FAILURE == result.getStatus())
        {
            Screenshot.captureScreenShot(x);
            printInfoMessage("Screenshot Captured");
        }
    }
}
