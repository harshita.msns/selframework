package framework;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Wait extends Driver {
    protected static WebDriverWait wait;

    public static WebDriverWait getInstanceOfWait() {
        wait = new WebDriverWait(driver, 30);
        return wait;
    }

    public void waitUntilElementIsClickable(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitUntilTitleContains(String title) {
        wait.until(ExpectedConditions.titleContains(title));
    }
}
