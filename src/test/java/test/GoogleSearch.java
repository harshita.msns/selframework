package test;

import framework.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;


public class GoogleSearch extends BaseTest {

    public static final String THOUGHTWORKS = "ThoughtWorks";

    @Test(groups = {"SmokeTest"})
    public void testGoogleSearch() {
        waitUntilTitleContains("Google");
        getSearchPage().searchInGoogle(THOUGHTWORKS);
        waitUntilTitleContains(THOUGHTWORKS);
        Assert.assertTrue(getTitle().contains(THOUGHTWORKS), "Title doesn't contain Thoughtworks");
        String textLink = getSearchPage().getLink(1).getText();
        Assert.assertTrue(textLink.contains(THOUGHTWORKS));
        printInfoMessage("Asserted statement");
    }
}
