package pages;

import framework.Wait;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.List;

public class SearchPage extends Wait {
    @FindBy(name = "q")
    private WebElement searchTextField;

    @FindBy(css = ".LC20lb.DKV0Md")
    private List<WebElement> listOfLinks;

    public SearchPage() {
        PageFactory.initElements(driver, this);
    }

    public void searchInGoogle(String searchText) {
        waitUntilElementIsClickable(searchTextField);
        searchTextField.click();
        searchTextField.sendKeys(searchText);
        searchTextField.sendKeys(Keys.ENTER);

    }

    public WebElement getLink(int linkNumber) {
        return listOfLinks.get(linkNumber - 1);
    }
}
